<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ColumnController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BoardController;
use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('logout', [AuthController::class, 'logout']);
        Route::post('refresh', [AuthController::class, 'refresh']);
        Route::post('me', [AuthController::class, 'me']);
    });
});

Route::group(['middleware' => 'auth:api', 'prefix' => 'users'], function () {
    Route::patch('updatePassword', [UserController::class, 'updatePassword']);
    Route::post('image', [UserController::class, 'image']);
});

Route::group(['middleware' => 'auth:api', 'prefix' => 'boards'], function () {
    Route::get('/{board}', [BoardController::class, 'get']);
    Route::get('/', [BoardController::class, 'index']);
    Route::post('/', [BoardController::class, 'create']);
    Route::put('/{board}', [BoardController::class, 'update']);
    Route::delete('/{board}', [BoardController::class, 'destroy']);
    Route::post('/{board}/image', [BoardController::class, 'image']);
});

Route::group(['middleware' => 'auth:api', 'prefix' => 'columns'], function () {
    Route::post('/', [ColumnController::class, 'create']);
    Route::put('/{column}', [ColumnController::class, 'update']);
    Route::delete('/{column}', [ColumnController::class, 'destroy']);
    Route::put('/{column}/draggable', [ColumnController::class, 'draggable']);
});

Route::group(['middleware' => 'auth:api', 'prefix' => 'tasks'], function () {
    Route::post('/', [TaskController::class, 'create']);
    Route::put('/{task}', [TaskController::class, 'update']);
    Route::delete('/{task}', [TaskController::class, 'destroy']);
});

Route::group(['middleware' => 'auth:api', 'prefix' => 'comments'], function () {
    Route::get('/{task}', [CommentController::class, 'get']);
    Route::post('/', [CommentController::class, 'create']);
    Route::delete('/{comment}', [CommentController::class, 'destroy']);
});
