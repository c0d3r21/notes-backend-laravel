<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function test_successful_registration(): void
    {
        $response = $this->register();
        $response->assertStatus(201);
    }

    public function test_successful_authorization(): void
    {
        $user_response = $this->register();
        $this->login($user_response->json('email'));

        $this->assertAuthenticated();
    }

    public function test_logout(): void
    {
        $user_response = $this->register();
        $this->login($user_response->json('email'));
        $response = $this->json('GET', '/api/auth/logout');

        $response->assertStatus(200);
    }

    public function test_get_user_information(): void
    {
        $user_response = $this->register();
        $login_response = $this->login($user_response->json('email'));

        $user_info_response = $this->json('POST', '/api/auth/me', [
            'access_token' => $login_response->json('access_token')
        ]);

        $user_info_response->assertStatus(200);
    }

    public function test_refresh_user_token(): void
    {
        $user_response = $this->register();
        $login_response = $this->login($user_response->json('email'));
        $refresh_response = $this->json('POST', '/api/auth/refresh', [
            'access_token' => $login_response->json('access_token')
        ]);

        $refresh_response->assertStatus(200);
    }

    public function register(): \Illuminate\Testing\TestResponse
    {
        $response = $this->json('POST', '/api/auth/register', [
            'username' => $this->faker->unique()->firstName(),
            'email' => $this->faker->unique()->email(),
            'password' => 'password'
        ]);

        return $response;
    }

    public function login($email, $password = 'password'): \Illuminate\Testing\TestResponse
    {
        $response = $this->json('POST', '/api/auth/login', [
            'email' => $email,
            'password' => $password
        ]);

        return $response;
    }
}
