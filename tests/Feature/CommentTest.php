<?php

namespace Tests\Feature;

use App\Models\Comment;
use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CommentTest extends TestCase
{
    use refreshDatabase;

    public function test_successful_create_comment(): void
    {
        $task = Task::factory()->create();

        $response = $this
            ->actingAs(User::find($task->column->board->author_id))
            ->postJson('/api/comments/', [
                'text' => 'testText',
                'task_id' => $task->id,
            ]);

        $response->assertStatus(200);
    }

    public function test_check_access_for_create_comment(): void
    {
        $task = Task::factory()->create();
        $user2 = User::factory()->create();

        $response = $this->actingAs($user2)->postJson('/api/comments/', [
            'text' => 'testText',
            'task_id' => $task->id,
        ]);

        $response->assertStatus(403);
    }

    public function test_validating_create_task_request(): void
    {
        $user = User::factory()->create();
        $task = Task::factory()->create();

        $response1 = $this->actingAs($user)->postJson('/api/comments/', [
            'text' => '',
            'task_id' => $task->id,
        ]);

        $response2 = $this->actingAs($user)->postJson('/api/comments/', [
            'text' => 'testText',
            'task_id' => -1,
        ]);

        $response1->assertStatus(422);
        $response2->assertStatus(422);
    }

    public function test_successful_destroy_comment(): void
    {
        $comment = Comment::factory()->create();

        $response = $this
            ->actingAs(User::find($comment->task->column->board->author_id))
            ->deleteJson('/api/comments/'.$comment->id);

        $response->assertStatus(204);
    }

    public function test_check_access_for_destroy_comment(): void
    {
        $comment = Comment::factory()->create();
        $user2 = User::factory()->create();
        $response = $this->actingAs($user2)->deleteJson('/api/comments/'.$comment->id);

        $response->assertStatus(403);
    }
}
