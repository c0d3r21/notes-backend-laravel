<?php

namespace Tests\Feature;

use App\Models\Board;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class BoardTest extends TestCase
{
    use refreshDatabase;

    public function test_successful_get_all_boards(): void
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->getJson('/api/boards/');

        $response->assertStatus(200);
    }

    public function test_successful_get_board(): void
    {
        $user = User::factory()->create();
        $board = Board::factory()->create();
        $response = $this->actingAs($user)->getJson('/api/boards/'.$board->id);

        $response->assertStatus(200);
    }

    public function test_check_access_for_get_the_board(): void
    {
        // if create board and table `User` is empty, automatically will be created new user and set it `id` in board->author_id
        $board = Board::factory()->create();
        $user = User::factory()->create();
        $response = $this->actingAs($user)->getJson('/api/boards/'.$board->id);

        $response->assertStatus(403);
    }

    public function test_successful_create_board(): void
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->postJson('/api/boards/', ['name' => 'test']);

        $response->assertStatus(200);
    }

    public function test_validating_create_board(): void
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->postJson('/api/boards/', ['name' => '']);

        $response->assertStatus(422);
    }

    public function test_validating_update_board(): void
    {
        $user = User::factory()->create();
        $board = Board::factory()->create();
        $response = $this->actingAs($user)->putJson('/api/boards/'.$board->id, ['name' => '', 'users' => []]);

        $response->assertStatus(422);
    }

    public function test_successful_update_board_name(): void
    {
        $user = User::factory()->create();
        $board = Board::factory()->create();
        $response = $this->actingAs($user)->putJson('/api/boards/'.$board->id, ['name' => 'successTestUpdateBoardName']);

        $response->assertStatus(200);
    }

    public function test_successful_update_board_users(): void
    {
        $user = User::factory()->create();
        $board = Board::factory()->create();
        $user2 = User::factory()->create();
        $response = $this->actingAs($user)->putJson('/api/boards/'.$board->id, ['users' => [$user2->username]]);

        $response->assertStatus(200);
    }

    public function test_check_access_for_update_board(): void
    {
        $board = Board::factory()->create();
        $user2 = User::factory()->create();
        $response = $this->actingAs($user2)->putJson('/api/boards/'.$board->id, ['name' => 'successTestCheckAccessToUpdateBoard']);

        $response->assertStatus(403);
    }

    public function test_successful_destroy_board(): void
    {
        $user = User::factory()->create();
        $board = Board::factory()->create();
        $response = $this->actingAs($user)->deleteJson('/api/boards/'.$board->id);

        $response->assertStatus(204);
    }

    public function test_check_access_for_destroy_board(): void
    {
        $board = Board::factory()->create();
        $user2 = User::factory()->create();
        $response = $this->actingAs($user2)->deleteJson('/api/boards/'.$board->id);

        $response->assertStatus(403);
    }

    public function test_successful_change_board_image(): void
    {
        $user = User::factory()->create();
        $board = Board::factory()->create();

        $response = $this
            ->actingAs($user)
            ->postJson('/api/boards/'.$board->id.'/image',
                [
                    'image' => UploadedFile::fake()
                        ->image('test.jpg')
                        ->size(300)
                ]);

        $response->assertStatus(200);
    }

    public function test_validating_image_size_for_board_image(): void
    {
        $user = User::factory()->create();
        $board = Board::factory()->create();

        $response = $this
            ->actingAs($user)
            ->postJson('/api/boards/'.$board->id.'/image', [
                'image' => UploadedFile::fake()
                    ->image('test.jpg')
                    ->size(1000)
            ]);

        $response->assertStatus(422);
    }

    public function test_validating_image_format_for_board_image(): void
    {
        $user = User::factory()->create();
        $board = Board::factory()->create();

        $response = $this
            ->actingAs($user)
            ->postJson('/api/boards/'.$board->id.'/image', [
                'image' => UploadedFile::fake()
                    ->image('test.txt')
                    ->size(300)
            ]);

        $response->assertStatus(422);
    }
}
