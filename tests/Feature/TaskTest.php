<?php

namespace Tests\Feature;

use App\Models\Column;
use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TaskTest extends TestCase
{
    use refreshDatabase;

    public function test_successful_create_task(): void
    {
        $column = Column::factory()->create();

        $response = $this
            ->actingAs(User::find($column->board->author_id))
            ->postJson('/api/tasks/', [
                'title' => 'testTitle',
                'position' => 1,
                'column_id' => $column->id,
            ]);

        $response->assertStatus(200);
    }

    public function test_check_access_for_create_task(): void
    {
        $column = Column::factory()->create();
        $user2 = User::factory()->create();

        $response = $this->actingAs($user2)->postJson('/api/tasks/', [
            'title' => 'testTitle',
            'position' => 2,
            'column_id' => $column->id,
        ]);

        $response->assertStatus(403);
    }

    public function test_successful_update_task_title(): void
    {
        $task = Task::factory()->create();

        $response = $this
            ->actingAs(User::find($task->column->board->author_id))
            ->putJson('/api/tasks/'.$task->id, [
                'title' => 'testTitle',
            ]);

        $response->assertStatus(200);
    }

    public function test_successful_update_task_position(): void
    {
        $task = Task::factory()->create();
        $task2 = Task::factory()->create();

        $response = $this
            ->actingAs(User::find($task->column->board->author_id))
            ->putJson('/api/tasks/'.$task->id, [
                'position' => $task2->position,
            ]);

        $response->assertStatus(200);
    }

    public function test_check_access_for_update_task(): void
    {
        $task = Task::factory()->create();
        $user2 = User::factory()->create();

        $response = $this->actingAs($user2)->putJson('/api/tasks/'.$task->id, [
            'title' => 'testTitle',
            'position' => 2,
        ]);

        $response->assertStatus(403);
    }

    public function test_validating_update_task_request(): void
    {
        $user = User::factory()->create();
        $task = Task::factory()->create();

        $response1 = $this->actingAs($user)->putJson('/api/tasks/'.$task->id, [
            'title' => '',
            'position' => 2,
        ]);

        $response2 = $this->actingAs($user)->putJson('/api/tasks/'.$task->id, [
            'title' => 'test',
            'position' => null,
        ]);

        $response1->assertStatus(422);
        $response2->assertStatus(422);
    }

    public function test_validating_create_task_request(): void
    {
        $user = User::factory()->create();
        $column = Column::factory()->create();

        $response1 = $this->actingAs($user)->postJson('/api/tasks/', [
            'title' => '',
            'position' => 2,
            'column_id' => $column->id
        ]);

        $response2 = $this->actingAs($user)->postJson('/api/tasks/', [
            'title' => 'test',
            'position' => '',
            'column_id' => $column->id
        ]);

        $response3 = $this->actingAs($user)->postJson('/api/tasks/', [
            'title' => 'test',
            'position' => 2,
            'column_id' => ''
        ]);

        $response1->assertStatus(422);
        $response2->assertStatus(422);
        $response3->assertStatus(422);
    }

    public function test_successful_destroy_task(): void
    {
        $task = Task::factory()->create();

        $response = $this
            ->actingAs(User::find($task->column->board->author_id))
            ->deleteJson('/api/tasks/'.$task->id);

        $response->assertStatus(204);
    }

    public function test_check_access_for_destroy_task(): void
    {
        $task = Task::factory()->create();
        $user2 = User::factory()->create();
        $response = $this->actingAs($user2)->deleteJson('/api/tasks/'.$task->id);

        $response->assertStatus(403);
    }
}
