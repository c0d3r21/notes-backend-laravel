<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function test_successful_change_password(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->patchJson('/api/users/updatePassword', [
            "old_password" => 'password',
            "new_password" => 'password123',
        ]);

        $response->assertStatus(200);
    }

    public function test_fail_change_password_if_incorrect_old_password(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->patchJson('/api/users/updatePassword', [
            "old_password" => 'password123',
            "new_password" => 'password1234',
        ]);

        $response->assertStatus(422);
    }

    public function test_fail_change_password_if_the_old_and_new_password_are_not_different(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->patchJson('/api/users/updatePassword', [
            "old_password" => 'password',
            "new_password" => 'password',
        ]);

        $response->assertStatus(422);
    }

    public function test_change_user_image(): void
    {
        $user = User::factory()->create();
        Storage::fake('avatars');

        $response = $this->actingAs($user)->postJson('/api/users/image', [
            "image" => UploadedFile::fake()->image('imageForTest999.jpg')->size(1000),
        ]);

        if (empty($user->getFirstMedia('profile')))
        {
            $response->assertStatus(422);
        }
        else if($user->getFirstMedia('profile')->name === 'imageForTest999')
        {
            $url = explode("/", $response->json('image'));
            $url = $url[2].'/'.$url[3].'/'.$url[4];
            Storage::disk('public')->assertExists($url);
            $response->assertStatus(200);
        }

    }
}
