<?php

namespace Tests\Feature;

use App\Models\Board;
use App\Models\Column;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ColumnTest extends TestCase
{
    use refreshDatabase;

    public function test_successful_create_column(): void
    {
        $board = Board::factory()->create();

        $response = $this
            ->actingAs(User::find($board->author_id))
            ->postJson('/api/columns/', [
                'title' => 'testTitle',
                'position' => 2,
                'board_id' => $board->id,
            ]);

        $response->assertStatus(200);
    }

    public function test_successful_update_column_title(): void
    {
        $column = Column::factory()->create();

        $response = $this
            ->actingAs(User::find($column->board->author_id))
            ->putJson('/api/columns/'.$column->id, [
                'title' => 'testTitle',
            ]);

        $response->assertStatus(200);
    }

    public function test_successful_update_column_position(): void
    {
        $column = Column::factory()->create();
        $column2 = Column::factory()->create();

        $response = $this
            ->actingAs(User::find($column->board->author_id))
            ->putJson('/api/columns/'.$column->id, [
                'position' => $column2->position,
            ]);

        $response->assertStatus(200);
    }

    public function test_check_access_for_create_column(): void
    {
        $board = Board::factory()->create();
        $user2 = User::factory()->create();

        $response = $this->actingAs($user2)->postJson('/api/columns/', [
            'title' => 'testTitle',
            'position' => 2,
            'board_id' => $board->id,
        ]);

        $response->assertStatus(403);
    }

    public function test_check_access_for_update_column(): void
    {
        $column = Column::factory()->create();
        $user2 = User::factory()->create();

        $response = $this->actingAs($user2)->putJson('/api/columns/'.$column->id, [
            'title' => 'testTitle',
            'position' => 2,
        ]);

        $response->assertStatus(403);
    }

    public function test_validating_update_column_request(): void
    {
        $user = User::factory()->create();
        $column = Column::factory()->create();

        $response1 = $this->actingAs($user)->putJson('/api/columns/'.$column->id, [
            'title' => '',
            'position' => 2,
        ]);

        $response2 = $this->actingAs($user)->putJson('/api/columns/'.$column->id, [
            'title' => 'test',
            'position' => null,
        ]);

        $response1->assertStatus(422);
        $response2->assertStatus(422);
    }

    public function test_validating_create_column_request(): void
    {
        $user = User::factory()->create();
        $board = Board::factory()->create();

        $response1 = $this->actingAs($user)->postJson('/api/columns/', [
            'title' => '',
            'position' => 2,
            'board_id' => $board->id
        ]);

        $response2 = $this->actingAs($user)->postJson('/api/columns/', [
            'title' => 'test',
            'position' => '',
            'board_id' => $board->id
        ]);

        $response3 = $this->actingAs($user)->postJson('/api/columns/', [
            'title' => 'test',
            'position' => 2,
            'board_id' => ''
        ]);

        $response1->assertStatus(422);
        $response2->assertStatus(422);
        $response3->assertStatus(422);
    }

    public function test_successful_destroy_column(): void
    {
        $column = Column::factory()->create();
        $response = $this
            ->actingAs(User::find($column->board->author_id))
            ->deleteJson('/api/columns/'.$column->id);

        $response->assertStatus(204);
    }

    public function test_check_access_for_destroy_column(): void
    {
        $column = Column::factory()->create();
        $user2 = User::factory()->create();
        $response = $this->actingAs($user2)->deleteJson('/api/columns/'.$column->id);

        $response->assertStatus(403);
    }
}
