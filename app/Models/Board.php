<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Board extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = ['name', 'author_id'];

    public function columns(): HasMany
    {
        return $this->hasMany(Column::class);
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this
            ->addMediaConversion('preview')
            ->fit(Manipulations::FIT_CROP, 500, 500)
            ->nonQueued();
    }

    public function authorOrUser(User $user): bool
    {
        if($this->isAuthor($user)) {
            return true;
        }

        if($this->users->firstWhere('id', $user->id) !== null) {
            return true;
        }

        return false;
    }

    public function isAuthor(User $user): bool
    {
        if ($user->id === $this->author_id) {
            return true;
        } else {
            return false;
        }
    }
}
