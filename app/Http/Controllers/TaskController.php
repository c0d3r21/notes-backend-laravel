<?php

namespace App\Http\Controllers;

use App\Http\Requests\DraggableTaskRequest;
use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Http\Resources\BoardResource;
use App\Http\Resources\ColumnResource;
use App\Http\Resources\TaskResource;
use App\Models\Board;
use App\Models\Column;
use App\Models\Task;
use Illuminate\Http\JsonResponse;

class TaskController extends Controller
{
    public function create(CreateTaskRequest $request): JsonResponse
    {
        $data = $request->validated();
        $user = auth()->user();
        $column = Column::find($data['column_id']);

        if ($column->board->authorOrUser($user)) {
            $task = Task::create($data);

            return response()->json(new TaskResource($task));
        }

        return response()->json(null, 403);
    }

    public function update(UpdateTaskRequest $request, Task $task): JsonResponse
    {
        $data = $request->validated();
        $user = auth()->user();

        if ($task->column->board->authorOrUser($user)) {
            if (isset($data['title'])) {
                $task->title = $data['title'];
                $task->save();

                return response()->json(new TaskResource($task));
            }

            // update task position
            if (isset($data['position'])) {
                if ($task->position === $data['position']) {
                    return response()->json(null, 400);
                }

                if ($task->position < $data['position']) {
                    $tasks = Task::where([
                        ['column_id', '=', $task->column_id],
                        ['position', '>', $task->position],
                        ['position', '<=', $data['position']],
                    ])->get()->all();

                    foreach ($tasks as $el) {
                        $el->position--;
                        $el->save();
                    }

                    $task->position = $data['position'];
                    $task->save();
                    return response()->json(new ColumnResource($task->column));
                }

                if ($task->position > $data['position']) {
                    $tasks = Task::where([
                        ['column_id', '=', $task->column_id],
                        ['position', '>=', $data['position']],
                        ['position', '<', $task->position],
                    ])->get()->all();

                    foreach ($tasks as $el) {
                        $el->position++;
                        $el->save();
                    }

                    $task->position = $data['position'];
                    $task->save();
                    return response()->json(new ColumnResource($task->column));
                }
            }
        }
        return response()->json(null, 403);
    }

    public function destroy(Task $task): JsonResponse
    {
        $user = auth()->user();
        if ($task->column->board->authorOrUser($user)) {
            $task->delete();

            return response()->json(null, 204);
        }

        return response()->json(null, 403);
    }
}
