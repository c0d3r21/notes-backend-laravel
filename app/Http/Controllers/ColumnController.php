<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateColumnRequest;
use App\Http\Requests\UpdateColumnRequest;
use App\Http\Requests\DraggableColumnRequest;
use App\Http\Resources\BoardResource;
use App\Http\Resources\ColumnResource;
use App\Models\Board;
use App\Models\Column;
use Illuminate\Http\JsonResponse;

class ColumnController extends Controller
{
    public function create(CreateColumnRequest $request): JsonResponse
    {
        $data = $request->validated();
        $user = auth()->user();
        $board = Board::find($data['board_id']);

        if($board->authorOrUser($user)) {
            $column = Column::create($data);

            return response()->json(new ColumnResource($column));
        }

        return response()->json(null, 403);
    }

    public function update(UpdateColumnRequest $request, Column $column): JsonResponse
    {
        $data = $request->validated();
        $user = auth()->user();
        if($column->board->authorOrUser($user)) {
            if (isset($data['title'])) {
                $column->title = $data['title'];
                $column->save();
                return response()->json(new ColumnResource($column));
            }

            if (isset($data['position'])) {
                //update column position
                if ($column->position === $data['position']) {
                    return response()->json(null, 400);
                }

                if ($column->position < $data['position']) {
                    $columns = Column::where([
                        ['board_id', '=', $column->board_id],
                        ['position', '>', $column->position],
                        ['position', '<=', $data['position']],
                    ])->get()->all();

                    foreach ($columns as $el) {
                        $el->position--;
                        $el->save();
                    }

                    $column->position = $data['position'];
                    $column->save();
                    return response()->json(new BoardResource($column->board));
                }

                if ($column->position > $data['position']) {
                    $columns = Column::where([
                        ['board_id', '=', $column->board_id],
                        ['position', '>=', $data['position']],
                        ['position', '<', $column->position],
                    ])->get()->all();

                    foreach ($columns as $el) {
                        $el->position++;
                        $el->save();
                    }

                    $column->position = $data['position'];
                    $column->save();
                    return response()->json(new BoardResource($column->board));
                }
            }
        }

        return response()->json(null, 403);
    }

    public function destroy(Column $column): JsonResponse
    {
        $user = auth()->user();
        if($column->board->authorOrUser($user)) {
            $column->delete();

            return response()->json(null, 204);
        }

        return response()->json(null, 403);
    }
}
