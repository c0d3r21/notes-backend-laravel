<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImageRequest;
use App\Http\Requests\UpdatePasswordRequest;
use App\Http\Resources\UserResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function updatePassword(UpdatePasswordRequest $request): JsonResponse
    {
        $data = $request->validated();
        $user = auth()->user();
        $user->password = Hash::make($data['new_password']);
        $user->save();
        return response()->json();
    }

    public function image(ImageRequest $request): JsonResponse
    {
        $user = auth()->user();
        $file = $request->files->get('image');

        if(!empty($user->getFirstMedia('profile'))) {
            $user->getFirstMedia('profile')->delete();
        }

        $user->addMedia($file)->toMediaCollection('profile');
        $user->refresh();

        return response()->json(new UserResource($user));
    }
}
