<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateBoardRequest;
use App\Http\Requests\CreateBoardRequest;
use App\Http\Requests\ImageRequest;
use App\Http\Resources\AllBoardResource;
use App\Http\Resources\BoardResource;
use App\Models\Board;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;

class BoardController extends Controller
{
    public function index(): JsonResponse
    {
        /** @var Collection $boards */
        /** @var User $user */

        $user = auth()->user();
        $boards = $user->boards;
        $authorBoards = $user->authorBoards;
        $result = $boards->merge($authorBoards);

        return response()->json(AllBoardResource::collection($result));
    }

    public function get(Board $board): JsonResponse
    {
        /** @var User $user */
        $user = auth()->user();

        if($board->authorOrUser($user)) {
            return response()->json(new BoardResource($board));
        }

        return response()->json(null, 403);
    }

    public function create(CreateBoardRequest $request): JsonResponse
    {
        /** @var User $user */
        $user = auth()->user();
        $data = $request->validated();

        $board = Board::create([
            'name' => $data['name'],
            'author_id' => $user->id
        ]);

        return response()->json(new BoardResource($board));
    }

    public function update(UpdateBoardRequest $request, Board $board): JsonResponse
    {
        /** @var User $user */
        $data = $request->validated();
        $user = auth()->user();

        if ($board->isAuthor($user)) {
            if (isset($data['users'])) {
                if (end($data['users']) === $user->username) {
                    return response()->json(['error' => 'You are the author of this board'], 400);
                }
                $setUsers = User::query()->whereIn('username', $data['users'])->get();
                $board->users()->sync($setUsers);

                return response()->json(new BoardResource($board));
            }

            if (isset($data['name'])) {
                $board->name = $data['name'];
                $board->save();

                return response()->json(new BoardResource($board));
            }
        }

        return response()->json('You don`t have access', 403);
    }

    public function destroy(Board $board): JsonResponse
    {
        /** @var User $user */
        $user = auth()->user();
        if ($board->isAuthor($user)) {
            $board->delete();
            return response()->json(null, 204);
        }

        return response()->json(null, 403);
    }

    public function image(ImageRequest $request, Board $board): JsonResponse
    {
        $file = $request->files->get('image');

        if (!empty($board->getFirstMedia('board'))) {
            $board->getFirstMedia('board')->delete();
        }

        $board->addMedia($file)->toMediaCollection('board');
        $board->refresh();

        return response()->json(new BoardResource($board));
    }
}
