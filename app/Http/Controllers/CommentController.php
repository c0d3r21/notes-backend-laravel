<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Http\Resources\CommentResource;
use App\Http\Resources\ShortUserResource;
use App\Models\Comment;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class CommentController extends Controller
{
    public function get(Task $task): JsonResponse
    {
        $user = auth()->user();

        if($task->column->board->authorOrUser($user)) {
            return response()->json(CommentResource::collection($task->comments));
        }

        return response()->json(null, 403);
    }

    public function create(CreateCommentRequest $request): JsonResponse
    {
        $data = $request->validated();
        $task = Task::find($data['task_id']);
        $user = auth()->user();

        if($task->column->board->authorOrUser($user)) {
            $data['user_id'] = $user->id;
            $comment = Comment::create($data);

            return response()->json(new CommentResource($comment));
        }

        return response()->json(null, 403);
    }

    public function destroy(Comment $comment): JsonResponse
    {
        $user = auth()->user();
        if($comment->task->column->board->isAuthor($user) || $comment->user_id === $user->id) {
            $comment->delete();

            return response()->json(null, 204);
        }

        return response()->json(null, 403);
    }
}
