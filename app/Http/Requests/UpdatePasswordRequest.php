<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Closure;

class UpdatePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'new_password' => 'required|min:6|different:old_password',
            'old_password' => ['required', 'min:6', function (string $attribute, mixed $value, Closure $fail)
            {
                $user = auth()->user();
                if(!Hash::check($this->old_password, $user->getAuthPassword()))
                {
                    $fail('The old password is incorrect');
                }
            }],
        ];
    }
}
