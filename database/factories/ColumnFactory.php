<?php

namespace Database\Factories;

use App\Models\Board;
use App\Models\Column;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Column>
 */
class ColumnFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     * @throws \Exception
     */
    public function definition(): array
    {
        $board_id = Board::all()->first() ? Board::all()->random()->id : Board::factory()->create()->id;
        $max_position = Column::where('board_id', '=', $board_id)->max('position');
        if (!isset($max_position)) {
            $max_position = 1;
        }
        else {
            $max_position++;
        }

        return [
            'title' => Str::random(10),
            'board_id' => $board_id,
            'position' => $max_position,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
