<?php

namespace Database\Factories;

use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends Factory<\App\Models\Comment>
 */
class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $random_task_id = Task::all()->first() ? Task::all()->random() : Task::factory()->create()->id;

        return [
            'text' => Str::random(20),
            'user_id' => User::all()->first() ? User::all()->random()->id : User::factory()->create()->id,
            'task_id' => $random_task_id,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
