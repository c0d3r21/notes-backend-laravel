<?php

namespace Database\Factories;

use App\Models\Column;
use App\Models\Task;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Task>
 */
class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $column_id = Column::all()->first() ? Column::all()->random()->id : Column::factory()->create()->id;
        $max_position = Task::select('position')->where('column_id', '=', $column_id)->max('position');

        return [
            'title' => Str::random(10),
            'position' => $max_position ? $max_position + 1 : 1,
            'column_id' => $column_id,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
