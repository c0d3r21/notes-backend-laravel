<?php

namespace Database\Seeders;

 use App\Models\Comment;
 use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    use WithoutModelEvents;

    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            UserSeeder::class,
            BoardSeeder::class,
            BoardUserSeeder::class,
            ColumnSeeder::class,
            TaskSeeder::class,
            CommentSeeder::class,
        ]);
    }
}
